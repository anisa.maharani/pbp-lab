from django.urls import path
from .views import index, xml, json

urlpatterns = [
    path('', index, name='notes_index'),
    path('xml', xml, name='notes_xml'),
    path('json', json, name='notes_json'),
]
