#Lab 2 Answers
###1. Apakah perbedaan antara JSON dan XML?
JSON: <br>
- Berdasarkan dari JavaScript
- Tidak menggunakan tag akhir
- Mendukung penggunaan array
- Tidak mendukung penggunaan comment
- Encoding yang didukung hanya UTF-8
- Cara merepresentasikan __object__
- JavaScript Object Notation (JSON)

XML:

- Berasal dari SGML (Standard Generalized Markup Language)
- Struktur syntax menggunakan tag
- Tidak mendukung menggunaan array
- Dapat mendukung banyak tipe encoding
- Cara merepresentasikan __data__
- Extensible Markup Language

> https://www.geeksforgeeks.org/difference-between-json-and-xml/

###2. Apakah perbedaan antara HTML dan XML?
HTML:
- Hyper Text Markup Language
- Tidak case sensitive
- Tag penutup tidak perlu
- Untuk memperlihatkan data
- Tidak membawa data. Hanya memperlihatkan
- Tag pada html sudah didefinisi

XML
- Extensible Markup Language
- Case sensitive
- Tag penutup diperlukan
- Untuk menyimpan data
- Membawa data dari dan ke database
- Tag didefinisi oleh user

> https://www.geeksforgeeks.org/html-vs-xml/