from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=30, primary_key=True)
    dari = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    message = models.TextField()