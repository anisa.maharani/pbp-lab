from django.db import models
import datetime

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30, primary_key=True)
    npm = models.BigIntegerField(default=0)
    dob = models.DateField(default=datetime.date.today)

    # TODO Implement missing attributes in Friend model
