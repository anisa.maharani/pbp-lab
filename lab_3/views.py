from django.shortcuts import render, redirect
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect

@login_required(login_url='/admin/login/')
# Create your views here.
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

def add_friend(request):
    context = {}
    form = FriendForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect(index)

    context['form'] = form
    return render(request, "lab3_form.html", context)