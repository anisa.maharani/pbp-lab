from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from lab_2.models import Note

# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)

